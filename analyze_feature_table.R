# Take a first look at August's data from Norflox expt 1
# Caveats : RNA was poor conc ; diversity was low because of freezing WW

# TODO : normalize total read counts 

# Prelims ----

library(ALDEx2)
source('./0-general_functions_main.R') # source libraries etc.
# select <- dplyr::select # assign precedence to the dplyr select function that is overwritten by MASS::.. run if needed

flnm <- 'formated_feature-table_Norflox expt 1.csv'

# Read data ----

# raw_counts <- readxl::read_xlsx(flnm, # load data - selected by August
#                                 range = 'A1:U40') 

raw_counts <- read_csv(flnm) %>% # selected by August and formatted by Kiara

  # clean column headers
  rename_with(.fn = ~ str_replace_all(.x, ' ng/µl Norfloxacin ', '_')) %>% 
  
  # Store the finest classification of taxa
  rowwise() %>% 
  mutate(taxonomic_classification =  # get's the last match classification
           # str_match_all(Taxa, ';([fgs]__[:alpha:]*)') %>% # family, genus or species
           str_split(Taxa, ';') %>% .[[1]] %>% # split each level ; flatten the list
           .[str_detect(., '[:alpha:]')] %>% tail(1) # detect non empty classification ; select last one
           ) %>% 
  
  # Make long format 
  pivot_longer(cols = 3:12, # columns with the '_'
               names_to = c('group', 'replicate'), names_sep = '_', 
               values_to = 'abundance') %>% 
  
  # calculate stats
  group_by(Amplicon, group, Taxa) %>% # average of the 3 technical replicates
  mutate(mean = mean(abundance), stdev = sd(abundance), cv = stdev/mean) %>% 
  ungroup() # remove grouping again



# check the classification level
raw_counts %>% 
  select(taxonomic_classification) %>% 
  mutate(classification_level = str_extract(taxonomic_classification, '^(.)')) %>% 
  rowwise() %>%
  count(classification_level)


total_reads <- 
  select(raw_counts, -Taxa) %>% 
  group_by(Amplicon, group, replicate) %>% # columns : "Amplicon"  "group"     "replicate" 
  
  summarise(total_reads = sum(abundance))

# reshape data for differential abundance analysis
raw_wide <- 
  select(raw_counts, 
         Amplicon, taxonomic_classification, group, replicate, abundance) %>% # select relevant columns
  filter(group != 'Blank') %>%  # remove blanks
  
  pivot_wider(names_from = c(group, replicate) , values_from = abundance) # wider data
  
# move classification into rownames
# rownames(raw_wide) <- raw_wide$taxonomic_classification
  
bc_total_wide <- 
  select(raw_counts, 
         Amplicon, taxonomic_classification, group, replicate, abundance) %>% # select relevant columns
  filter(group != 'Blank') %>%  # remove blanks
  
  pivot_wider(names_from = c(group, replicate, Amplicon) , values_from = abundance) # wider data


  
norflox_presence <- c(rep('absent', 3), rep('present', 6))
norflox_conc <- rep(c('0', '12.5', '50'), each = 3) # make a vector for column grouping

amplicons <- c('Barcoded', 'Total')

amplicon_list <- rep(amplicons, each = 9)

# Differential abundance ----

ttest_presence_list <- map(amplicons, 
  ~ aldex(reads = filter(raw_wide, Amplicon == .x) %>% 
          select(-Amplicon, -taxonomic_classification),
        conditions = norflox_presence,
        test = 't', effect = TRUE, paired.test = FALSE)
)

# Questions : what are the rows in this table? There are only 14, 
# where the original data had 36, but only 14 had non zeros for each group

ttest_barcode_16s <- 
  aldex(reads = bc_total_wide %>% 
          select(-taxonomic_classification),
        conditions = amplicon_list,
        test = 't', effect = TRUE, paired.test = FALSE)

# Plot summary of statistical test

# Bland-Altmann (MA) plots
aldex.plot(ttest_presence_list[[1]], # Barcoded
           type="MA", test="welch", xlab="Log-ratio abundance",
           ylab="Difference")

aldex.plot(ttest_presence_list[[2]],  # Total
           type="MA", test="welch", xlab="Log-ratio abundance",
           ylab="Difference")

aldex.plot(ttest_barcode_16s,  # barcoded vs 16s
           type="MA", test="welch", xlab="Log-ratio abundance",
           ylab="Difference")

# Effect plot
aldex.plot(ttest_presence_list[[2]], 
           type="MW", test="welch", xlab="Dispersion",
           ylab="Difference")



# Plot exploratory ----

# plot total reads
ggplot(total_reads, aes(group, total_reads, colour = replicate)) + 
  geom_point() + 
  facet_wrap(facets = vars(Amplicon))

# plot reads per OTU, connected by lines ; excluding blanks
plt_readsbyotu <-
  {ggplot(filter(raw_counts, group != 'Blank'), 
        aes(group, abundance, label = taxonomic_classification)) + 
  geom_point() + 
  geom_line(aes(group = interaction(replicate, Taxa))) + 
  facet_wrap(facets = vars(Amplicon))} %>%  
  format_logscale_y()

ggsave(plot_as('norflox_1_abundance-by-otu'), width = 5, height = 3)

plotly::ggplotly(plt_readsbyotu)


# plot CV per OTU, connected by lines ; excluding blanks :: Use for picking least variable housekeeping taxa
plt_cvbyotu <-
  ggplot(filter(raw_counts, group != 'Blank'), 
          aes(group, cv, label = taxonomic_classification)) + 
      geom_point() + 
      geom_line(aes(group = interaction(replicate, Taxa))) + 
      facet_wrap(facets = vars(Amplicon))

ggsave(plot_as('norflox_1_cv-by-otu'), width = 5, height = 3)

plotly::ggplotly(plt_cvbyotu, dynamicTicks = T)


# plot stdev per OTU, connected by lines ; excluding blanks :: Use for picking least variable housekeeping taxa
plt_stdevbyotu <-
  {ggplot(filter(raw_counts, group != 'Blank'), 
         aes(group, stdev, label = taxonomic_classification)) + 
  geom_point() + 
  geom_line(aes(group = interaction(replicate, Taxa))) + 
  facet_wrap(facets = vars(Amplicon)) } %>% 
  format_logscale_y()
  
plotly::ggplotly(plt_stdevbyotu)


